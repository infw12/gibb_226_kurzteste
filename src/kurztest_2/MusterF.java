package kurztest_2;

/**
 * 
 * Michael Senn
 * infw12 2A
 * 25.02.2013
 * Serie F
 * 
 */

public class MusterF {
	
	public static void main(String[] args) {
		
		muster(3);
		System.out.println();
		muster(2);
	}
	
	public static void muster(int laenge) {
				
		for (int x = 0; x < laenge * 2; x++) {
			for (int y = 0; y < laenge * 2; y++) {
				if (x == y) {
					//System.out.println(String.format("Putting a symbol at x: %s y: %s", x, y));
					System.out.print("\\");
				}
				else if((x + y) == (laenge * 2 - 1)) {
					//System.out.println(String.format("Putting a symbol at x: %s y: %s", x, y));
					System.out.print("/");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}		
	}
}
